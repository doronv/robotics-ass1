Author: Doron Cohen

**How to use this package?**

1. Clone this project into your catkin_ws/src folder.
2. Run catkin_make clean && install.
3. Remember to source the setup.bash before every rosrun command.
4. run 'roscore'
5. run 'rossun ass1 circle-finder.py'  (3rd node)
6. run 'rossun ass1 circle-drawer.py'  (last node)
7. run 'rossun ass1 image-publisher.py'  (1st node)
8. run 'rossun ass1 color-publisher.py {0-255} {0-255} {0-255}', example: 'rossun ass1 color-publisher.py 255 0 0' for red  (1st node)
9. Watch if 'found_circle.jpg' being created at 'scripts' directory

The END