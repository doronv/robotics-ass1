#!/usr/bin/env python

import rospy
import numpy as np
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import cv2


#TODO: remove debug options
def impublisher():
    impub = rospy.Publisher('image', Image, queue_size=10)
    rospy.init_node('image_publisher', anonymous=True)
    cv_image = cv2.imread('./src/ass1/scripts/colors.jpg')
    # print(type(cv_image))
    # cv2.imshow('test',cv_image)
    try:
        im_msg = CvBridge().cv2_to_imgmsg(cv_image)
    except CvBridgeError as e:
        print(e)
    rospy.loginfo('Trying to publish image...')
    impub.publish(im_msg)
    rospy.loginfo('The image is being published')
    # rospy.loginfo('Press any key to stop publishing')
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    raw_input("Press Enter to close...")
    rospy.loginfo('Closing node')

if __name__ == '__main__':
    impublisher()