#!/usr/bin/env python

import rospy
import numpy as np
from sensor_msgs.msg import Image, RegionOfInterest
from cv_bridge import CvBridge, CvBridgeError
from std_msgs.msg import ColorRGBA
import cv2


class Server:
    def __init__(self):
        self.cv_image = None
        self.color = None
        self.cir_pub = rospy.Publisher('circle', RegionOfInterest, queue_size=10)

    def callback0(self, im_msg):
        rospy.loginfo('Image calllback initiated')
        self.cv_image = CvBridge().imgmsg_to_cv2(im_msg)
        
        cv2.imshow('GotImage:',self.cv_image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

        if self.color is not None:
            self.pubilsh_circle()
        

    def callback1(self, color):
        rospy.loginfo('Color calllback initiated')
        self.color = color

        if self.cv_image is not None:
            self.pubilsh_circle()

    def pubilsh_circle(self):
        gs_cv_image = cv2.cvtColor(self.cv_image, cv2.COLOR_BGR2GRAY)
        circles = cv2.HoughCircles(gs_cv_image, cv2.HOUGH_GRADIENT, 1.2, 120, minRadius=50)
        i = findbestmatchingcolor(self.cv_image, circles, self.color)
        location_of_circle = RegionOfInterest(int(circles[0][i][0]), int(circles[0][i][1]), int(circles[0][i][2]), int(circles[0][i][2]), True)
        self.cir_pub.publish(location_of_circle)


def findbestmatchingcolor(cv_image, circles, color):
    min_value = 100
    min_index = -1
    i = 0
    # circles[0] = np.round(circles[0]).astype(int)
    for circle in circles[0]:
        if (color.b-cv_image[circle[1]][circle[0]][0]/255.0)**2 + (color.g-cv_image[circle[1]][circle[0]][1]/255.0)**2 + (color.r-cv_image[circle[1]][circle[0]][2]/255.0)**2 < min_value: 
            min_index = i
            min_value = (color.b-cv_image[circle[1]][circle[0]][0]/255.0)**2 + (color.g-cv_image[circle[1]][circle[0]][1]/255.0)**2 + (color.r-cv_image[circle[1]][circle[0]][2]/255.0)**2
        i = i+1
    return min_index


def cirfinder():
    rospy.init_node('circle_finder', anonymous=True)
    rospy.loginfo("Started")
    server = Server()
    rospy.Subscriber('image', Image, server.callback0)
    rospy.Subscriber('color', ColorRGBA, server.callback1)
    rospy.spin()


if __name__ == '__main__':
    cirfinder()