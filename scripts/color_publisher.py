#!/usr/bin/env python

import rospy
import sys
from std_msgs.msg import ColorRGBA

def colpublisher():
    color = ColorRGBA(float(sys.argv[1])/255.0, float(sys.argv[2])/255.0, float(sys.argv[3])/255.0, 1.0); #TODO: get color from input
    pub = rospy.Publisher('color', ColorRGBA, queue_size=10)
    rospy.init_node('color_publisher', anonymous=True)
    rospy.loginfo('Publishing color...')
    pub.publish(color)
    raw_input("Press Enter to close...")

if __name__ == '__main__':
    colpublisher()