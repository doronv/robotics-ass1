#!/usr/bin/env python

import rospy
import numpy as np
from sensor_msgs.msg import Image, RegionOfInterest
from cv_bridge import CvBridge, CvBridgeError
from std_msgs.msg import ColorRGBA
import cv2
import os

class Server:
    def __init__(self):
        self.cv_image = None
        self.circle = None

    def callback0(self, im_msg):
        rospy.loginfo('Image calllback initiated (got some image)')
        self.cv_image = CvBridge().imgmsg_to_cv2(im_msg)
        
        # cv2.imshow('GotImage',self.cv_image)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        if self.circle is not None:
            self.save_circle()
        
    def callback1(self, circle):
        rospy.loginfo('Circle calllback initiated (got some circle)')
        self.circle = circle
        # print(circle)
        if self.cv_image is not None:
            self.save_circle()

    def save_circle(self):
        center = (self.circle.x_offset, self.circle.y_offset)
        radius = self.circle.height
        cv2.circle(self.cv_image, center, radius, (0, 0, 0), 3)

        cv2.imshow('CircleDrawed',self.cv_image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

        cv2.imwrite('./src/ass1/scripts/found_circle.jpg' ,self.cv_image)


def cirdraw():
    rospy.init_node('circle_drawer', anonymous=True)
    rospy.loginfo("Started")
    server = Server()
    print(os.getcwd())
    im_sub = rospy.Subscriber('image', Image, server.callback0)
    cir_sub = rospy.Subscriber('circle', RegionOfInterest, server.callback1)
    rospy.spin()


if __name__ == '__main__':
    cirdraw()
